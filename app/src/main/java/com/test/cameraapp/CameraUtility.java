package com.test.cameraapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.media.ImageReader;
import android.os.Handler;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

import java.util.Arrays;

/** This class has camera2 and drawing related utiity functions. */
public class CameraUtility {

    /** Open camera using CameraManager. */
    public static void openCamera(
            Activity activity,
            CameraDevice.StateCallback callback,
            Handler handler)
            throws CameraAccessException, SecurityException {
        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        manager.openCamera(manager.getCameraIdList()[0], callback, handler);
    }

    /** Create capture session to configure the CameraDevice for preview on the surface. */
    public static CaptureRequest.Builder openPreview(
            CameraDevice cam,
            TextureView textureView,
            ImageReader imageReader,
            CameraCaptureSession.StateCallback callback,
            Handler handler)
            throws CameraAccessException {
        Surface previewSurface = new Surface(textureView.getSurfaceTexture());
        Surface readerSurface = imageReader.getSurface();

        CaptureRequest.Builder builder = cam.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
        builder.addTarget(previewSurface);
        builder.addTarget(readerSurface);

        cam.createCaptureSession(
                Arrays.asList(previewSurface, readerSurface),
                callback,
                handler);
        return builder;
    }

    /** Start previewing continuously using the CameraCaptureSession. */
    public static void updatePreview(
            CameraCaptureSession session,
            CaptureRequest.Builder builder,
            Handler handler)
            throws CameraAccessException {
        builder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        session.setRepeatingRequest(builder.build(), null, handler); // XXX
    }

    /** Draw Red circle when threshold is reached. */
    public static void drawThreshold(Surface surface) {
        if (surface == null || !surface.isValid()) {
            Log.e(Constants.TAG, "Null/invalid surface!");
            return;
        }

        Canvas canvas = surface.lockCanvas(null);
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR); // clear the canvas
        canvas.drawCircle(100f, 100f, 10f, Constants.RED_CIRCLE_PAINT); // draw the circle
        surface.unlockCanvasAndPost(canvas);
    }

    /** clear the Surface of previous drawings. */
    public static void clearSurface(Surface surface) {
        if (surface == null || !surface.isValid()) {
            Log.e(Constants.TAG, "Null/invalid surface!");
            return;
        }

        Canvas canvas = surface.lockCanvas(null);
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        surface.unlockCanvasAndPost(canvas);
    }

    /** Draw keypoints on the surface. */
    public static void drawKeypoints(Surface surface, Bitmap destBitmap, Mat mat) {
        if (surface == null || !surface.isValid()) {
            Log.e(Constants.TAG, "Null/invalid surface!");
            return;
        }

        Utils.matToBitmap(mat, destBitmap);

        Canvas canvas = surface.lockCanvas(null);
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR); // clear the canvas
        canvas.drawBitmap(destBitmap, 0f, 0f, null); // draw the bitmap
        surface.unlockCanvasAndPost(canvas);
    }
}
