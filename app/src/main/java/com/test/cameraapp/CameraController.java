package com.test.cameraapp;

import android.graphics.Bitmap;
import android.media.Image;
import android.os.Handler;
import android.util.Log;
import android.view.TextureView;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.nio.ByteBuffer;

/** Class to control access to resources and background processing. */
public class CameraController {

    // app's resource context
    private final CameraResourceContext mContext;

    // reference to CameraCallbacks
    private final CameraCallbacks mCallback;

    private ImageConfig mConfig;

    public CameraController(CameraCallbacks callbacks) {
        mContext = CameraApplication.getInstance().getContext(); // set context once
        mCallback = callbacks;
    }

    /** acquire the required resources. */
    void acquire() {
        TextureView texture = mContext.getTextureView();
        if (texture != null) {
            texture.setSurfaceTextureListener(mCallback);
        }
    }

    /** release the acquired resources. */
    void release() {
        TextureView texture = mContext.getTextureView();
        if (texture != null && texture.isAvailable()) {
            texture.getSurfaceTexture().release();
        }
    }

    /** start background task of processing images. */
    public void start() {
        Handler handler = mContext.getHandler();
        if (handler != null) {
            handler.postDelayed(mProcessor, Constants.CAPTURE_PERIOD);
        }
    }

    /** stop background task of processing images. */
    public void stop() {
        Handler handler = mContext.getHandler();
        if (handler != null) {
            handler.removeCallbacks(mProcessor);
        }
    }

    /** feature detection in C++ using opencv. */
    private native int detect(ByteBuffer buffer, long nativeMatAddr);

    /** background task to capture images from TextureView, detect features and display them on SurfaceView. */
    private Runnable mProcessor = new Runnable() {

        @Override
        public void run() {
            Handler handler = mContext.getHandler();
            if (handler == null) {
                Log.w(Constants.TAG, "Null handler. Task is stopped!");
                return;
            }

            mConfig = mContext.getImageConfig(mConfig); // give up last ImageConfig and get the latest one
            Image image = mConfig.getImage(); // get hold of the latest image
            TextureView texture = mContext.getTextureView();

            if (image != null && texture != null && texture.isAvailable()) {
                int width = mConfig.getWidth();
                int height = mConfig.getHeight();

                // get YUV data from the image
                Image.Plane planeY = image.getPlanes()[0];
                Image.Plane planeU = image.getPlanes()[1];
                Image.Plane planeV = image.getPlanes()[2];
                int bytesY = planeY.getBuffer().remaining();
                int bytesU = planeU.getBuffer().remaining();
                int bytesV = planeV.getBuffer().remaining();

                // fill ByteBuffer with data of each planes
                ByteBuffer buffer = ByteBuffer.allocateDirect(bytesY + bytesU + bytesV);
                buffer.put(planeY.getBuffer());
                buffer.put(planeU.getBuffer());
                buffer.put(planeV.getBuffer());
                buffer.flip();

                // pass the ByteBuffer of the image to native side
                int keypoints = detect(buffer, mConfig.getMat().getNativeObjAddr());

                // Update SurfaceView with keypoints
                if (keypoints > Constants.MAX_KEYPOINTS_THRESHOLD) { // threshold reached
                    CameraUtility.drawThreshold(mContext.getSurface());
                } else if (keypoints > 0) { // draw the keypoints
                    CameraUtility.drawKeypoints(mContext.getSurface(), mConfig.getBitmap(), mConfig.getMat());
                } else { // just clear the surface of any previous drawing
                    CameraUtility.clearSurface(mContext.getSurface());
                }
            }

            // post this same task again after CAPTURE_PERIOD time
            handler.postDelayed(mProcessor, Constants.CAPTURE_PERIOD);
        }
    };
}
