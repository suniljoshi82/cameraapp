package com.test.cameraapp;

import android.graphics.Bitmap;
import android.media.Image;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

/** This class has data related to image acquired by ImageReader,
 *  like width/height/mat/bitmap which we will use in background processing.
 */
public class ImageConfig {

    private Image mImage; // image acquired by ImageReader
    private int mWidth; // width of the image
    private int mHeight; // height of the image

    // destination mat in which keypoints detected are written
    private Mat mMat;
    // bitmap used to draw mMat SurfaceView
    private Bitmap mBitmap;

    public ImageConfig() {
        mImage = null;
        mWidth = 0;
        mHeight = 0;
        mMat = null;
        mBitmap = null;
    }

    // close the image
    public void close() {
        if (mImage != null) {
            mImage.close();
        }
        mImage = null;
    }

    // set data used by C code
    private native void setDimensions(int width, int height);

    /** get the image last acquired. */
    public Image getImage() {
        return mImage;
    }

    /** set the image newly acquired. */
    public void setImage(Image image) {
        close(); // clean previous image

        if (image == null) {
            return;
        }

        mImage = image;
        int width = image.getWidth();
        int height = image.getHeight();

        // update other data only when width or height has changed
        if (width != mWidth || height != mHeight) {
            mWidth = width;
            mHeight = height;
            setDimensions(mWidth, mHeight);

            if (mBitmap != null) {
                mBitmap.recycle(); // release
            }
            mBitmap = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);

            if (mMat != null) {
                mMat.release(); // release
            }
            mMat = new Mat(mHeight, mWidth, CvType.CV_8UC4);
        }
    }

    public Mat getMat() {
        return mMat;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public int getHeight() {
        return mHeight;
    }

    public int getWidth() {
        return mWidth;
    }
}
