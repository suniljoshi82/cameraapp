package com.test.cameraapp;

import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CaptureRequest;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.TextureView;

import java.nio.ByteBuffer;

/** Common class to register TextureView, SurfaceHolder and Camera2 related callbacks. */
public class CameraCallbacks implements TextureView.SurfaceTextureListener, SurfaceHolder.Callback, ImageReader.OnImageAvailableListener {

    /** app's resource context */
    private final CameraResourceContext mContext;

    /** CaptureRequest Builder to be used for updating preview on TextureView */
    private CaptureRequest.Builder mBuilder;

    public CameraCallbacks() {
        mContext = CameraApplication.getInstance().getContext(); // set the context once
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        try { // try to open camera
            Handler handler = mContext.getHandler();
            if (handler == null) {
                Log.w(Constants.TAG, "null handler");
            } else {
                CameraUtility.openCamera(mContext.getActivity(), mDeviceCb, handler);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
            Log.e(Constants.TAG, "Exception opening camera!");
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        // release the resources specific to camera and TextureView
        mContext.setCameraCaptureSession(null);
        mContext.setCameraDevice(null);
        mContext.setTextureView(null);
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mContext.setSurface(holder.getSurface()); // update the resource
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        mContext.setSurface(holder.getSurface()); // update the resource
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mContext.setSurface(null); // release the resource
    }

    /** Callback to get updates about state of CameraDevice. */
    private CameraDevice.StateCallback mDeviceCb = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            TextureView texture = mContext.getTextureView();
            if (texture == null) {
                return;
            }

            CameraDevice cameraResource = null;
            ImageReader imageReader = ImageReader.newInstance(
                    texture.getWidth(),
                    texture.getHeight(),
                    ImageFormat.YUV_420_888,
                    3);
            imageReader.setOnImageAvailableListener(CameraCallbacks.this, mContext.getHandler());

            try { // try to configure camera to capture images
                if (texture.isAvailable()) {
                    mBuilder = CameraUtility.openPreview(
                            camera, texture, imageReader, mCaptureCb, mContext.getHandler());
                    cameraResource = camera;
                } else {
                    Log.w(Constants.TAG, "TextureView is not available");
                }
            } catch (CameraAccessException e) {
                e.printStackTrace();
                Log.e(Constants.TAG, "Exception opening preview!");
            }

            mContext.setCameraDevice(cameraResource); // update the resource
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            mContext.setCameraDevice(null); // release the resource
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            mContext.setCameraDevice(null); // release the resource
        }
    };

    /** Callback to get updates about states of CameraCaptureSession. */
    private CameraCaptureSession.StateCallback mCaptureCb = new CameraCaptureSession.StateCallback() {

        @Override
        public void onConfigured(@NonNull CameraCaptureSession session) {
            CameraCaptureSession sessionResource = null;

            try { // try to start capturing images using this session
                Handler handler = mContext.getHandler();
                if (mBuilder == null || handler == null) {
                    Log.i(Constants.TAG, "Null builder/handler");
                } else {
                    CameraUtility.updatePreview(session, mBuilder, handler);
                    sessionResource = session;
                }
            } catch (CameraAccessException e) {
                e.printStackTrace();
                Log.e(Constants.TAG, "Exception in capture session!");
            }

            mContext.setCameraCaptureSession(sessionResource); // update the resource
        }

        @Override
        public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
            Log.e(Constants.TAG, "CameraCaptureSession configuration failed!");

            mContext.setCameraCaptureSession(null); // release the resource
        }
    };

    @Override
    public void onImageAvailable(ImageReader reader) {
        Image image = reader.acquireLatestImage();
        if (image == null) {
            return;
        }

        mContext.setImageConfig(image); // update imageConfig
    }
}
