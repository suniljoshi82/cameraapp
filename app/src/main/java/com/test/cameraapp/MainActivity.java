package com.test.cameraapp;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.widget.Toast;

import org.opencv.android.OpenCVLoader;

/** Main page of the app. */
public class MainActivity extends AppCompatActivity {

    static {
        // load and initialize opencv before loading our lib
        if (OpenCVLoader.initDebug()) {
            System.loadLibrary("native-lib");
        } else {
            throw new RuntimeException("Failed to load opencv lib!");
        }
    }

    // app related resource context
    private CameraResourceContext mContext;

    // controller instance
    private CameraController mController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = CameraApplication.getInstance().getContext();
        mContext.setActivity(this); // set resource

        setContentView(R.layout.activity_main);
        TextureView texture = (TextureView) findViewById(R.id.cam_view);
        mContext.setTextureView(texture); // set resource

        CameraCallbacks callback = new CameraCallbacks();
        mController = new CameraController(callback);

        // configure surfaceView
        SurfaceView surface = (SurfaceView) findViewById(R.id.over_view);
        surface.setZOrderOnTop(true);
        SurfaceHolder holder = surface.getHolder();
        holder.setFormat(PixelFormat.TRANSLUCENT);
        holder.addCallback(callback);
    }

    @Override
    public void onStart() {
        super.onStart();

        // controller to acquire resources only when sufficient permissions
        if (checkPermissions()) {
            mController.acquire();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        // start bg thread, then ask the controller to start processing
        startBackgroundThread();
        mController.start();
    }

    @Override
    public void onPause() {
        super.onPause();

        // ask controller to stop and then stop bg thread
        mController.stop();
        stopBackgroundThread();
    }

    @Override
    public void onStop() {
        super.onStop();

        // release resources before stopping app
        mController.release();
    }

    /** check permission to access camera. */
    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[] {Manifest.permission.CAMERA},
                    Constants.REQUEST_CAMERA_PERMISSION_CODE);
        }

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Constants.REQUEST_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                // permission denied, close the app
                Toast.makeText(this, "Not enough permissions", Toast.LENGTH_LONG).show();

                mContext.setActivity(null); // release resource
                finish(); // close the app
            }
        }
    }

    // background thread on which camera callbacks and background image processing will happen
    private HandlerThread mThread;

    /** start the background thread */
    private void startBackgroundThread() {
        stopBackgroundThread();

        mThread = new HandlerThread("background-thread");
        mThread.start();
        mContext.setHandler(new Handler(mThread.getLooper()));
    }

    /** stop the background thread */
    private void stopBackgroundThread() {
        if (mThread != null) {
            mThread.quit();
            mThread = null;
            mContext.setHandler(null);
        }
    }
}
