package com.test.cameraapp;

import android.app.Activity;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.media.Image;
import android.os.Handler;
import android.view.Surface;
import android.view.TextureView;

import java.lang.ref.WeakReference;

/** Class to contain all app specific resources as WeakReferences
 * so they are released as and when they are not used anymore. */
public class CameraResourceContext {

    private WeakReference<Activity> mActivity;
    private WeakReference<CameraCaptureSession> mCamSession;
    private WeakReference<TextureView> mTextureView;
    private WeakReference<Surface> mSurface;
    private WeakReference<Handler> mHandler;
    private WeakReference<CameraDevice> mCameraDevice;
    private ImageConfig mImageConfig = new ImageConfig();

    public void setActivity(Activity activity) {
        mActivity = new WeakReference<Activity>(activity);
    }

    public Activity getActivity() {
        if (mActivity == null) {
            return null;
        }
        return mActivity.get();
    }

    public void setCameraCaptureSession(CameraCaptureSession session) {
        CameraCaptureSession oldSession = getCameraCaptureSession();
        mCamSession = new WeakReference<CameraCaptureSession>(session);

        if (oldSession != null) {
            try {
                oldSession.stopRepeating();
            } catch (CameraAccessException e) {
                e.printStackTrace();
            } finally {
                oldSession.close();
            }
        }
    }

    public CameraCaptureSession getCameraCaptureSession() {
        if (mCamSession == null) {
            return null;
        }
        return mCamSession.get();
    }

    public void setTextureView(TextureView textureView) {
        mTextureView = new WeakReference<>(textureView);
    }

    public TextureView getTextureView() {
        if (mTextureView == null) {
            return null;
        }
        return mTextureView.get();
    }

    public void setSurface(Surface surface) {
        mSurface = new WeakReference<Surface>(surface);
    }

    public Surface getSurface() {
        if (mSurface == null) {
            return null;
        }
        return mSurface.get();
    }

    public void setHandler(Handler handler) {
        mHandler = new WeakReference<Handler>(handler);
    }

    public Handler getHandler() {
        if (mHandler == null) {
            return null;
        }
        return mHandler.get();
    }

    public void setCameraDevice(CameraDevice camera) {
        CameraDevice oldCamera = getCameraDevice();
        if (oldCamera == camera) {
            return;
        }

        if (oldCamera != null) {
            oldCamera.close();
        }

        mCameraDevice = new WeakReference<CameraDevice>(camera);
    }

    public CameraDevice getCameraDevice() {
        if (mCameraDevice == null) {
            return null;
        }
        return mCameraDevice.get();
    }

    /** get the latest imageConfig, give up the old imageConfig. */
    public synchronized ImageConfig getImageConfig(ImageConfig old) {
        ImageConfig newOne = mImageConfig;

        if (old == null) {
            mImageConfig = new ImageConfig();
        } else {
            mImageConfig = old;
        }

        return newOne;
    }

    /** Set the image config with the newly acquired image. */
    public synchronized void setImageConfig(Image image) {
        if (mImageConfig == null) {
            mImageConfig = new ImageConfig();
        }

        mImageConfig.setImage(image); // update image
    }
}
