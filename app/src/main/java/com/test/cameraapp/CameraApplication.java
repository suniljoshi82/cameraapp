package com.test.cameraapp;

import android.app.Application;

/** Holds CameraResourceContext singleton specific to current app instance. */
public class CameraApplication extends Application {

    /** singleton instance */
    private static CameraApplication sInstance;

    /** current app's resources */
    private final CameraResourceContext mContext = new CameraResourceContext();

    @Override
    public void onCreate() {
        super.onCreate();

        /* set the instance */
        sInstance = this;
    }

    public static CameraApplication getInstance() {
        if (sInstance == null) {
            throw new RuntimeException("MyCameraApp is not initialized yet!");
        }

        return sInstance;
    }

    /** get the resource context */
    public CameraResourceContext getContext() {
        return mContext;
    }
}
