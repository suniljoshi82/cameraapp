package com.test.cameraapp;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

/** All constants used across the app classes are here. */
public class Constants {

    // Tag for logging
    static final String TAG = "MyCameraApp";

    // requestcode to access camera
    static final int REQUEST_CAMERA_PERMISSION_CODE = 9999;

    // delay between two background tasks to capture, detect and draw
    static final int CAPTURE_PERIOD = 100;

    // max number of keypoints to be drawn
    static final int MAX_KEYPOINTS_THRESHOLD = 25;

    // Paint object used to draw circle
    static final Paint RED_CIRCLE_PAINT = new Paint();

    static { // configure RED_CIRCLE_PAINT
        Constants.RED_CIRCLE_PAINT.setStyle(Paint.Style.FILL_AND_STROKE);
        Constants.RED_CIRCLE_PAINT.setColor(Color.RED);
        Constants.RED_CIRCLE_PAINT.setStrokeWidth(5);
    }
}
