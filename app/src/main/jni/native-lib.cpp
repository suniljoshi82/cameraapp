#include <jni.h>
#include <string>
#include "/Users/SunilJoshi/workspace/my/projects/OpenCV-android-sdk/sdk/native/jni/include/opencv2/core.hpp"
#include "/Users/SunilJoshi/workspace/my/projects/OpenCV-android-sdk/sdk/native/jni/include/opencv/cv.hpp"

// detector instance
static cv::Ptr<cv::SimpleBlobDetector> detector = cv::SimpleBlobDetector::create();

// keypoints vector
static std::vector<cv::KeyPoint> keypoints;

// BGR mat
static cv::Mat * pBgr = 0;

// width and height set by ImageConfig.java
static int sWidth;
static int sHeight;

extern "C" JNIEXPORT void JNICALL
Java_com_test_cameraapp_ImageConfig_setDimensions(
        JNIEnv *env, jobject thiz, jint width, jint height) {
    sWidth = width;
    sHeight = height;

    if (pBgr) {
        pBgr->release();
        delete pBgr;
    }

    pBgr = new cv::Mat(sHeight, sWidth, CV_8UC4);
}

extern "C" JNIEXPORT jint JNICALL
Java_com_test_cameraapp_CameraController_detect(
        JNIEnv* env, jobject thiz, jobject buffer, jlong nativeMatAddr) {
    // 1. Get the image byte[]
    uchar* data = (uchar*) env->GetDirectBufferAddress(buffer);
    if (!data) {
        return -1;
    }

    // 2. create YUV mat using the ByteBuffer passed
    cv::Mat yuv(sHeight * 3 / 2, sWidth, CV_8UC1, data);
    // 3. convert YUV mat to BGR format
    cv::cvtColor(yuv, *pBgr, CV_YUV420sp2BGRA);
    // 4. release YUV mat
    yuv.release();

    // 5. clear previous keypoints
    keypoints.clear();

    // 6. detect using SimpleBlobDetector
    detector->detect(*pBgr, keypoints);

    // 7. draw the keypoints
    cv::Mat * pDesMat = (cv::Mat *) nativeMatAddr;

#if 0 // Draw only keypoints
    *pDesMat = cv::Scalar(0, 0, 0, 0); // clear previous mat data
#else // Draw complete BGR image with keypoints
    *pDesMat = pBgr->clone();
#endif

    for(size_t i = 0; i < keypoints.size(); i++ ) {
        circle(                               // draw circle
                *pDesMat, // use passed mat to draw on
                cv::Point(keypoints[i].pt.x, keypoints[i].pt.y),
                10,
                cv::Scalar(0, 0, 255, 255),
                3);                           // width
    }

    return keypoints.size();
}
